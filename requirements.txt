setuptools~=73.0.1
certifi==2024.7.4
opensearch-py~=2.7.1
pytest~=7.4.2
requests~=2.32.3
jsonlines~=4.0.0
