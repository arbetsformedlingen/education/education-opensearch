import pytest
import os
import jsonlines
from education_opensearch.opensearch_store import OpensearchStore

@pytest.mark.unit
def test_store_doc():
    # Reads a json newline file from this dir and store documents to Open search.
    # The file must contain dicts that open search can index, e.g. consequent use of data type for a given field.
    root_dir = os.path.dirname(os.path.abspath(__file__))
    json_file_path = os.path.join(root_dir, 'scraper_output.jl')
    opensearch_store = OpensearchStore()
    opensearch_store.start_new_save('unit-test-from-file')

    with jsonlines.open(json_file_path) as reader:
        for dict in reader:
            opensearch_store.save_education_to_repository(dict)